#
## 使用一个循环来持续提示用户输入路径
#while [[ "$asset_dir" != "exit" ]]; do
#    read -p "需要检查的文件的路径: " asset_dir
#    # 检查用户是否输入了 'exit'
#    if [[ "$asset_dir" == "exit" ]]; then
#        echo "退出输入。"
#        break
#    fi
#    # 这里可以添加代码来处理用户输入的路径
#    echo "您的路径是: $asset_dir"
#    break
#done
#
#




#!/bin/bash


## 假设文件目录如下
#A/
#|-- B1/
#| |-- C1
#| |-- C2
#|-- B2
#|-- B3
#|-- Self


# 获取当前脚本的位置路径（Self）
SCRIPT_PATH=$(realpath "$0")

# 获取当前脚本所在目录的路径（A/Self >>> A）
SCRIPT_DIR=$(dirname "$SCRIPT_PATH")

# 获取路径上一级目录的路径（xx/A >>> xx）
PARENT_DIR=$(dirname "$SCRIPT_DIR")

imagesetsPath=$SCRIPT_DIR
savePath="${imagesetsPath}/EXImages"

echo "$imagesetsPath"

################# --- 删除文件夹中的文件
#if [ -d "$savePath" ]; then
#    # 如果路径是目录，遍历目录并删除文件
#    for file in "$savePath"/*; do
#        if [ -f "$file" ]; then
#            rm "$file"
#        fi
#    done
#else
#    # 如果路径是单个文件，直接删除
#    if [ -f "$savePath" ]; then
#        rm "$savePath"
#    else
#        echo "路径不存在或不是文件也不是目录。"
#    fi
#fi

################# --- 删除文件夹
if [ -d "$savePath" ]; then
    rm -r "$savePath"
    echo "文件夹 $savePath 已被删除。"
else
    echo "文件夹 $savePath 不存在。"
fi


################# --- 原位修改图片名称
#for folder in "$imagesetsPath"/*/; do
#    if [ -d "$folder" ]; then
#        echo "🟢 🔎 🟢 🔎 🟢 🔎 🟢  $folder"
#        # 提取 folderName（$folder 文件夹的名称去掉后缀）
#        folderName=${folder%.*}
#        folderName=${folderName##*/}
#        find "$folder" -type f | while read -r file; do
#            echo "$file"
#            
#            # 判断文件名格式并进行修改
#            # 将改名后的图片统一放到 imagesetsPath 下 的EXImages文件夹下 若EXImages不存在则创建。不需要将原来的文件删除
#            
#            
#            if [[ $file == *".png" ]]; then
#                baseName=${file%.*}
#                extension=${file##*.}
#                if [[ $baseName == *"@2x" ]]; then
#                    newName="$folderName@2x.$extension"
#                elif [[ $baseName == *"@3x" ]]; then
#                    newName="$folderName@3x.$extension"
#                else
#                    newName="$folderName.$extension"
#                fi
##                mv "$file" "${folder}/${newName}"
#                mv "$file" "${imagesetsPath}/EXImages/${newName}"
#
#            fi
#            
#        done
#    fi
#done


################# --- 保留原来的图片文件 ---- 将改名后的图片统一放到EXImages文件夹中
# 如果 EXImages 文件夹不存在则创建
mkdir -p "${imagesetsPath}/EXImages"

for folder in "$imagesetsPath"/*/; do
    
    if [ -d "$folder" ]; then
        echo "🟢 🔎 🟢 🔎 🟢 🔎 🟢  $folder"
        folderName=${folder%.*}
        folderName=${folderName##*/}
        find "$folder" -type f | while read -r file; do
            echo "$file"
            if [[ $file == *".png" ]]; then
                baseName=${file%.*}
                extension=${file##*.}
                if [[ $baseName == *"@2x" ]]; then {
                    newName="$folderName@2x.$extension"
                } elif [[ $baseName == *"@3x" ]]; then {
                    newName="$folderName@3x.$extension"
                } else {
                    newName="$folderName.$extension"
                }
                fi
                echo "${newName}"
#                cp "$file" "${imagesetsPath}/EXImages/${newName}"
                cp "$file" "${savePath}/${newName}"
            fi
            
        done
        
    fi
    
done


