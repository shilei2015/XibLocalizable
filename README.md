<!-- ***

![导入工具包776](img/2.jpg?ref_type=heads)

*** -->

[TOC]

# xib国际化文本提取

## 1.导入工具包

> 老黎版在LHLocalizable文件夹下
> 将工具包Localizable文件夹导入到项目目录中如下图结构

![导入工具包](img/1.jpg?ref_type=heads&inline=false)

            
## 2.执行国际化工具脚本

> 双击Localizable.command文件执行工具
### 若双击无法运行 执行下面代码
> chmod a+x {Localizable.command的路径}

### 修改本地化提取格式
<!-- ![结果内容](img/4.jpg?ref_type=heads&inline=false) -->
> 打开脚本后输入本地化文本格式， 比如 "LocalizableText".loc 则输入loc
> 老黎版直接回车是扫描全部硬编码的字符串
## 3.导出结果

![执行结果](img/2.jpg?ref_type=heads&inline=false)

![结果内容](img/3.jpg?ref_type=heads&inline=false)

## 工具下载

[https://gitlab.com/shilei2015/XibLocalizable.git](下载链接)


## 更新
### 2024-09-13
> 全面支持本地化文本提取 包括提取 [swift | .m | .xib | .storyBoard] 中的本地化文本。
自定义字符串文本提取如：swift中 localText = "Hello".loc   |   oc中  localText = @"Hello".loc
脚本执行时输入的字符则为：loc
![输入样式](img/5.jpg?ref_type=heads&inline=false)
