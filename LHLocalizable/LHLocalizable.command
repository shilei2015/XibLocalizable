#!/bin/bash


export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8
export LC_ALL=en_US.utf8


## 假设文件目录如下
#MyProject/
#|-- Documents/
#| |-- Report.docx
#| |-- Presentation.pptx
#|-- Logo.png
#|-- Shell.sh


# 获取当前脚本的路径（MyProject/Shell.sh）
SCRIPT_PATH=$(realpath "$0")
# 获取当前脚本所在目录的路径（MyProject）
SCRIPT_DIR=$(dirname "$SCRIPT_PATH")
# 获取上一级目录的路径（MyProject的上级）
PARENT_DIR=$(dirname "$SCRIPT_DIR")


searchFilesPath=$PARENT_DIR
#searchFilesPath='/Users/edy/Desktop/2024/Lumia/Lumia/Test.m'
savePath=$SCRIPT_DIR


#RESULT_DIR= "$savePath"/Result

tempPath_1="$savePath"/1-Current.strings
xibSortPath="$savePath"/xibSort.strings
tempPath_2="$savePath"/2-Append.strings
tempPath_3="$savePath"/3-Transform.strings
tempPath_4="$savePath"/4-Trans.strings
tempPath_5="$savePath"/5-deleteRepeat.strings

tempSwiftPath_1="$savePath"/1-SwiftCurrent.strings
tempSwiftPath_2="$savePath"/2-SwiftAppend.strings
tempSwiftPath_3="$savePath"/3-SwiftTransform.strings

swiftResult="$savePath"/SwiftResult.strings
tempSwiftPath_5="$savePath"/5-SwiftdeleteRepeat.strings

outResultPath="$savePath"/Localizable.strings
xibResultPath="$savePath"/XibLocalizable.strings

    
read -p "
-------------------------------------------
[请输入本地化字符串自定义格式如'text'.loc 则输入 loc]
:" localType


echo "
------------------------------------------- 查找 xib
"

#遍历searchFilesPath下所有包含.xib后缀的文件
#find "$searchFilesPath" -name "*.xib" -o -name "*.storyboard" -type f | while read xib_file; do
find "$searchFilesPath" \( -path "$searchFilesPath/Pods" -prune \) -o \( -name "*.xib" -o -name "*.storyboard" -type f \) | while read xib_file; do
    
    echo "🔎 xib >> $xib_file"
    #提取xib_file中的本地化文本
    ibtool --export-strings-file $tempPath_1 $xib_file
    iconv -f UTF-16 -t UTF-8 $tempPath_1 >> $tempPath_2
    reg='keyPath="placeHolder" value="[^"]*"'
    grep -H -o "$reg" $xib_file | awk -F: '{print $2}' >> $tempPath_3
    
done

grep 'value=' $tempPath_3 | sed -E 's/.*value="([^"]*)".*/"\1" = "\1";/' >> $xibSortPath


#/* Class = "UILabel"; text = "LocalLabel2"; ObjectID = "yO1-Kl-chZ"; */
#"yO1-Kl-chZ.text" = "LocalLabel2";
text=$(cat $tempPath_2)
#样式变换 >>>
echo "$text" | grep ' = ' | perl -ne 'if (/.*" = ("[^"].*\").*/) {print $1. qq( = ). $1. qq(;\n);}' >> $xibSortPath
sort $xibSortPath > $xibResultPath

echo "
------------------------------------------- 查找 .swift
"
#find "$searchFilesPath" -name "*.swift" -type f | while read swift_file; do
find "$searchFilesPath" \( -path "$searchFilesPath/Pods" -prune \) -o \( -name "*.swift" -type f \) | while read swift_file; do
   
    echo "🔎 swift >> $swift_file"
    #提取swift_file中的本地化文本
    reg='"[^"]*"'.${localType}
    if [ "" = "$localType"]; then
        reg='"[^"]*"'
    else
        reg='"[^"]*"'.${localType}
    fi
    echo "find reg: $reg"
    grep -H -o "$reg" $swift_file | awk -F: '{print $2}' >> $tempSwiftPath_2

done



echo "
------------------------------------------- 查找 .m
"
#find "$searchFilesPath" -name "*.m" -type f | while read m_file; do
find "$searchFilesPath" \( -path "$searchFilesPath/Pods" -prune \) -o \( -name "*.m" -type f \) | while read m_file; do
    

    echo "🔎 m >> $m_file"
    #提取m_file中的本地化文本
    reg='"[^"]*"'.${localType}
    if [ "" = "$localType"]; then
        reg='"[^"]*"'
    else
        reg='"[^"]*"'.${localType}
    fi
    echo "find reg: $reg"
    grep -H -o "$reg" $m_file | awk -F: '{print $2}' >> $tempSwiftPath_2

done

grep -H -o '"[^"].*\"' $tempSwiftPath_2 | awk -F: '{print $2 " = " $2 ";"}' | iconv -f utf-8 >> $tempSwiftPath_3

sort $tempSwiftPath_3 > $swiftResult

echo "\n//XibText\n\n" >> $tempPath_4
cat $xibResultPath >> $tempPath_4

echo "//CodeText\n\n" >> $tempPath_4
cat $swiftResult >> $tempPath_4

#cat $tempSwiftPath_3 >> $tempPath_2

echo "👌👌👌👌👌👌👌👌👌👌"

#text=$(cat $tempPath_2)

echo "提取国际化文本内容并调整格式为 \"XXX\"= \"XXX\""

##样式变换 >>>
#echo "$text" | grep ' = ' | perl -ne 'if (/.*" = ("[^"].*\").*/) {print $1. qq( = ). $1. qq(;\n);}' >> $tempPath_3

#提取样式 >>> "STText" = "STText";
#grep '"[^"].*\".* = "[^"].*\".*;' $tempPath_3 > $tempPath_4

#删除重复的内容
uniq $tempPath_4 > $tempPath_5

#删除特定内容的行(自己在项目中规定)
## 删除 "   " = "    ";  的行
#grep -v '"[ ]*" = "[ ]*";' $tempPath_5 > $outResultPath
## 删除 "   " = "👍🏻👍🏻👍🏻👍🏻";  的行
#grep -v '"[ ]*" = "👍🏻👍🏻👍🏻👍🏻";' $tempPath_5 > $outResultPath

#grep -v -e '"[ ]*" = "[ ]*";' -e '"[-]*" = "[-]*";' -e '"[ ]*" = "👍🏻👍🏻👍🏻👍🏻";' $tempPath_5 > $outResultPath

echo "内容提取完成 👌👌👌👌👌"

rm $tempPath_1
rm $xibSortPath
rm $tempPath_2
rm $tempPath_3
rm $tempPath_4
rm $tempPath_5
rm $tempSwiftPath_1
rm $tempSwiftPath_2
rm $tempSwiftPath_3
rm $swiftResult
rm $tempSwiftPath_5
rm $xibResultPath

echo '移除中间文件'
echo "退出 >>>> 请查看    Localizable.strings"
exit 0
