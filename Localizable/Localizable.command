#!/bin/bash


export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8
export LC_ALL=en_US.utf8


## 假设文件目录如下
#MyProject/
#|-- Documents/
#| |-- Report.docx
#| |-- Presentation.pptx
#|-- Logo.png
#|-- Shell.sh


# 获取当前脚本的路径（MyProject/Shell.sh）
SCRIPT_PATH=$(realpath "$0")
# 获取当前脚本所在目录的路径（MyProject）
SCRIPT_DIR=$(dirname "$SCRIPT_PATH")
# 获取上一级目录的路径（MyProject的上级）
PARENT_DIR=$(dirname "$SCRIPT_DIR")

searchFilesPath=$PARENT_DIR
#searchFilesPath='/Users/edy/Desktop/2024/Lumia/Lumia/Test.m'
savePath=$SCRIPT_DIR



#read -p "
#-------------------------------------------
#[请输入本地化字符串自定义格式如'text'.loc 则输入 loc]
#:" localType


localType = "nativeText"


############################################################
############################################################
echo "------------------------------------------- 查找 xib"
path_xib_current="$savePath"/xib-Current.strings
path_xib_Appent="$savePath"/xib-Append.strings
path_xib_Transform="$savePath"/xib-Transform.strings
path_xibResult="$savePath"/Xib_Result.strings

rm $path_xib_current
rm $path_xib_Appent
rm $path_xib_Transform
rm $path_xibResult


############################################################    .xib .storyboard
############################################################- start
#遍历searchFilesPath下所有包含.xib后缀的文件
#find "$searchFilesPath" -name "*.xib" -o -name "*.storyboard" -type f | while read xib_file; do
find "$searchFilesPath" \( -path "$searchFilesPath/Pods" -prune \) -o \( -name "*.xib" -o -name "*.storyboard" -type f \) | while read xib_file; do
    
    echo "🔎 xib >> $xib_file"
    
    #提取xib_file中的本地化文本
    ibtool --export-strings-file $path_xib_current $xib_file
    
    iconv -f UTF-16 -t UTF-8 $path_xib_current  >> $path_xib_Appent
# 📣📣📣📣📣📣📣 >> 文本样式

#/* Class = "UILabel"; text = "🐶🐶🐶"; ObjectID = "fUq-GT-8ej"; */
#"fUq-GT-8ej.text" = "🐶🐶🐶";
    
done

echo "------------------------------------------- xib文本格式变换"

#样式变换 >>>  【    "fUq-GT-8ej.text" = "你好";   >>>   "你好" = "你好";    】
echo "$(cat $path_xib_Appent)" | grep ' = ' | perl -ne 'if (/.*" = ("[^"].*\").*/) {print $1. qq( = ). $1. qq(;\n);}' >> $path_xib_Transform
# 📣📣📣📣📣📣📣 >> 文本样式

#删除重复 并 排序
sort $path_xib_Transform | uniq > $path_xibResult

echo "------------------------------------------- xib文本 处理完成"

rm $path_xib_current
rm $path_xib_Appent
rm $path_xib_Transform



############################################################    .xib .storyboard
############################################################- end
 
 
 
############################################################    .swift
############################################################- start
echo "------------------------------------------- 查找 .swift"

temp_Swift_Append="$savePath"/swift-Append.strings
temp_Swift_Transform="$savePath"/swift-Transform.strings
path_swift_Result="$savePath"/swift_Result.strings

rm $temp_Swift_Append
rm $temp_Swift_Transform
rm $path_swift_Result


#find "$searchFilesPath" -name "*.swift" -type f | while read swift_file; do
find "$searchFilesPath" \( -path "$searchFilesPath/Pods" -prune \) -o \( -name "*.swift" -type f \) | while read swift_file; do
   
    echo "🔎 swift >> $swift_file"
    
    #提取swift_file中的本地化文本
    reg='"[^"]*"'.${localType}
    if [ "" = "$localType"]; then
        reg='"[^"]*"'
    fi

    grep -H -o "$reg" $swift_file | awk -F: '{print $2}' >> $temp_Swift_Append
    # 📣📣📣📣📣📣📣 >> 文本样式
    # "text".nativeText
    
done

#样式变换 >>>  【    "你好".nativeText   >>>   "你好" = "你好";    】
grep -H -o '"[^"]*"' $temp_Swift_Append | awk -F: '{print $2 " = " $2 ";"}' | iconv -f utf-8 >> $temp_Swift_Transform

#删除重复的内容
sort $temp_Swift_Transform | uniq > $path_swift_Result

echo "------------------------------------------- swift 处理完成"

rm $temp_Swift_Append
rm $temp_Swift_Transform

############################################################    .swift
############################################################- end



############################################################    .m
############################################################- start
echo "------------------------------------------- 查找 .m"

temp_m_Append="$savePath"/m-Append.strings
temp_m_Transform="$savePath"/m-Transform.strings
path_m_Result="$savePath"/m_Result.strings

rm $temp_m_Append
rm $temp_m_Transform
rm $path_m_Result


#find "$searchFilesPath" -name "*.m" -type f | while read m_file; do
find "$searchFilesPath" \( -path "$searchFilesPath/Pods" -prune \) -o \( -name "*.m" -type f \) | while read m_file; do
   
    echo "🔎 m >> $m_file"
    
    #提取m_file中的本地化文本
    reg='"[^"]*"'.${localType}
    if [ "" = "$localType"]; then
        reg='"[^"]*"'
    fi
    grep -H -o "$reg" $m_file | awk -F: '{print $2}' >> $temp_m_Append
    # 📣📣📣📣📣📣📣 >> 文本样式
    # "text".nativeText
    
done

#样式变换 >>>  【    "你好".nativeText   >>>   "你好" = "你好";    】
grep -H -o '"[^"]*"' $temp_m_Append | awk -F: '{print $2 " = " $2 ";"}' | iconv -f utf-8 >> $temp_m_Transform

#删除重复的内容
sort $temp_m_Transform | uniq > $path_m_Result

echo "------------------------------------------- .m 处理完成"
rm $temp_m_Append
rm $temp_m_Transform
############################################################    .m
############################################################- end


############################################################    合并处理
############################################################- start
echo "------------------------------------------- 合并处理"
temp_Merge="$savePath"/Merge.strings
path_UndealResult="$savePath"/UnDeal-Result.strings
path_swift_xib_Result="$savePath"/Localizable.strings

rm $temp_Merge
rm $path_UndealResult
> $path_swift_xib_Result


echo " 
// ------ Xib Text -------

" >> $temp_Merge
cat $path_xibResult >> $temp_Merge

echo "
// ------ Swift Text -------

" >> $temp_Merge
cat $path_swift_Result >> $temp_Merge


echo "
// ------ m Text -------

" >> $temp_Merge
cat $path_m_Result >> $temp_Merge


echo "------------------------------------------- xib文本 与 swift文本 合并完成 并去重"
uniq $temp_Merge > $path_UndealResult

############################################################    合并处理
############################################################- end


#删除特定内容的行(自己在项目中规定)
## 删除 "   " = "    ";  的行
#grep -v '"[ ]*" = "[ ]*";' $tempPath_5 > $path_swift_Result
## 删除 "   " = "👍🏻👍🏻👍🏻👍🏻";  的行
#grep -v '"[ ]*" = "👍🏻👍🏻👍🏻👍🏻";' $tempPath_5 > $path_swift_Result

grep -v -e '"[ ]*" = "[ ]*";' -e '"[-]*" = "[-]*";' -e '"[ ]*" = "👍🏻👍🏻👍🏻👍🏻";' $path_UndealResult > $path_swift_xib_Result



echo "内容提取完成 👌👌👌👌👌"


rm $path_xibResult
rm $path_swift_Result
rm $path_m_Result

rm $temp_Merge
rm $path_UndealResult


echo '移除中间文件'
echo "退出 >>>> 请查看    Localizable.strings"
exit 0


