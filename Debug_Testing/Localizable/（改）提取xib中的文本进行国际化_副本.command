#!/bin/bash

## 目前实现了所有 xib上控件 国际化内容 提取、整理

#export LANG="zh_CN.UTF-8"
#export LC_ALL="zh_CN.UTF-8"
#export LC_ALL=zh_CN.GB2312
#export LANG=zh_CN.GB2312
#
#export LANG="en_US.UTF-8"
#export LANG=zh_CN.GBK
#export LC_ALL="en_US.UTF-8"
#export LC_ALL="C"


## 假设文件目录如下
#MyProject/
#|-- Documents/
#| |-- Report.docx
#| |-- Presentation.pptx
#|-- Logo.png
#|-- Shell.sh


# 获取当前脚本的路径（MyProject/Shell.sh）
SCRIPT_PATH=$(realpath "$0")
# 获取当前脚本所在目录的路径（MyProject）
SCRIPT_DIR=$(dirname "$SCRIPT_PATH")
# 获取上一级目录的路径（MyProject的上级）
PARENT_DIR=$(dirname "$SCRIPT_DIR")


xibSearchPath=$PARENT_DIR
savePath=$SCRIPT_DIR

tempPath_1="$savePath"/1-Current.strings
tempPath_2="$savePath"/2-Append.strings
tempPath_3="$savePath"/3-Transform.strings
tempPath_4="$savePath"/4-Trans.strings
tempPath_5="$savePath"/5-deleteRepeat.strings
outResultPath="$savePath"/Localizable.strings

iconv -f GBK -t UTF-8 $tempPath_3 -o $tempPath_3
#iconv -f GBK -t UTF-8 input.txt -o output.txt

echo "🎬🎬🎬🎬🎬🎬🎬🎬🎬🎬🎬🎬 $xibSearchPath"

#遍历xibSearchPath下所有包含.xib后缀的文件
find "$xibSearchPath" -name "*.xib" -type f | while read xib_file; do
    echo "$xib_file"
    
    #提取xib_file中的本地化文本
    ibtool --export-strings-file $tempPath_1 $xib_file
#/* Class = "UILabel"; text = "LocalLabel2"; ObjectID = "yO1-Kl-chZ"; */
#"yO1-Kl-chZ.text" = "LocalLabel2";
    cat $tempPath_1 >> $tempPath_2
    
done


echo "👌👌👌👌👌👌👌👌👌👌"

text=$(cat $tempPath_2)

echo "提取国际化文本内容并调整格式为 \"XXX\"= \"XXX\""

#样式变换 >>>

echo "$text" | grep '" = "' | sed -E 's/.*" = "([^"]*)".*/"\1" = "\1";/' > $tempPath_3




#echo "$text" | grep '" = "' > $tempPath_3

#提取样式 >>> "STText" = "STText";
#grep '"[^"]*" = "[^"]*";' $tempPath_3 > $tempPath_4

#删除重复的内容
sort $tempPath_3 | uniq > $tempPath_4

#删除特定内容的行(自己在项目中规定)
## 删除 "   " = "    ";  的行
#grep -v '"[ ]*" = "[ ]*";' $tempPath_5 > $outResultPath
## 删除 "   " = "👍🏻👍🏻👍🏻👍🏻";  的行
#grep -v '"[ ]*" = "👍🏻👍🏻👍🏻👍🏻";' $tempPath_5 > $outResultPath

grep -v -e '"[ ]*" = "[ ]*";' -e '"[-]*" = "[-]*";' -e '"[ ]*" = "👍🏻👍🏻👍🏻👍🏻";' $tempPath_4 > $outResultPath

echo "内容提取完成 👌👌👌👌👌"

rm $tempPath_1
rm $tempPath_2
rm $tempPath_3
rm $tempPath_4
rm $tempPath_5

echo '移除中间文件'
echo "退出 >>>> 请查看    Localizable.strings(❗️❗️中文会出现乱码 -- 暂时处理不了❗️❗️)"
exit 0
