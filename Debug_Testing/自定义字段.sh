#!/bin/bash


export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8


# 使用一个循环来持续提示用户输入路径
while [[ "$inPath" != "exit" ]]; do
    read -p "需要检查的文件的路径: " inPath
    # 检查用户是否输入了 'exit'
    if [[ "$inPath" == "exit" ]]; then
        echo "退出输入。"
        break
    fi
    # 这里可以添加代码来处理用户输入的路径
    echo "您的路径是: $inPath"
    break
done


# 使用一个循环来持续提示用户输入路径
while [[ "$outPath" != "exit" ]]; do
    read -p "输出文件的路径: " outPath
    # 检查用户是否输入了 'exit'
    if [[ "$outPath" == "exit" ]]; then
        echo "退出输入。"
        break
    fi
    # 这里可以添加代码来处理用户输入的路径
    echo "您的路径是: $outPath"
        break
done

# 提示用户是否开始处理
read -p "是否开始处理？(yes/no): " answer

# 检查用户是否输入了正确的值
if [ ! -f "$inPath" ]; then
    echo "输入文件不存在，请检查路径。"
    exit 1
fi

# 检查用户是否输入了正确的确认
if [ "$answer" != "yes" ]; then
    echo "操作已取消。"
    exit 0
fi


#inPath='/Users/edy/Desktop/XibLocalizable/自定义输入、输出路径/SwiftProject'
#outPath='/Users/edy/Desktop/XibLocalizable/自定义输入、输出路径/out'





tempPath_1="$outPath"/1-Current.strings
tempPath_2="$outPath"/2-Append.strings
tempPath_3="$outPath"/3-Transform.strings
tempPath_4="$outPath"/4-Trans.strings
tempPath_5="$outPath"/5-deleteRepeat.strings
outResultPath="$outPath"/Localizable.strings


echo "🎬🎬🎬🎬🎬🎬🎬🎬🎬🎬🎬🎬 $inPath"

#遍历inPath下所有包含.xib后缀的文件
find "$inPath" -name "*.xib" -type f | while read xib_file; do
    echo "$xib_file"
    
    #提取xib_file中的本地化文本
    ibtool --export-strings-file $tempPath_1 $xib_file
#/* Class = "UILabel"; text = "LocalLabel2"; ObjectID = "yO1-Kl-chZ"; */
#"yO1-Kl-chZ.text" = "LocalLabel2";
    cat $tempPath_1 >> $tempPath_2
    
done


echo "👌👌👌👌👌👌👌👌👌👌"

text=$(cat $tempPath_2)

echo "提取国际化文本内容并调整格式为 \"XXX\"= \"XXX\""

#样式变换 >>>

echo "$text" | grep ' = ' | sed -E 's/.*" = "([^"]*)".*/"\1" = "\1";/' > $tempPath_3

#提取样式 >>> "STText" = "STText";
grep '"[^"]*" = "[^"]*";' $tempPath_3 > $tempPath_4

#删除重复的内容
sort $tempPath_4 | uniq > $tempPath_5

#删除特定内容的行(自己在项目中规定)
## 删除 "   " = "    ";  的行
#grep -v '"[ ]*" = "[ ]*";' $tempPath_5 > $outResultPath
## 删除 "   " = "👍🏻👍🏻👍🏻👍🏻";  的行
#grep -v '"[ ]*" = "👍🏻👍🏻👍🏻👍🏻";' $tempPath_5 > $outResultPath

grep -v -e '"[ ]*" = "[ ]*";' -e '"[-]*" = "[-]*";' -e '"[ ]*" = "👍🏻👍🏻👍🏻👍🏻";' $tempPath_5 > $outResultPath

echo "内容提取完成 👌👌👌👌👌"

rm $tempPath_1
rm $tempPath_2
rm $tempPath_3
rm $tempPath_4
rm $tempPath_5

echo '移除中间文件'
echo "退出 >>>> 请查看    Localizable.strings"
exit 0

